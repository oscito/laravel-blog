## Blogas

Aprašymas:
Sistema yra blog'as.

Reikalingas funkcionalumas:
- Postų kategorijų valdymas (CRUD)
- Blog postų valdymas (CRUD) iš adminkės
- Postų rūšiavimas pagal kategorijas

Instrukcijos:
- Susikuriame naują Laravel projektą
  - Atsidarome terminalą ir su komanda "cd" nunaviguojame, kur norime laikyti projekto failus
  - Susikuriame direktoriją laravel-blog(komanda `mkdir laravel-blog`)
  - Nunaviguojame į laravel-blog folderį (komanda `cd laravel-blog`)
  - Instaliuojame Laravel (komanda `composer create-project . --prefer-dist`)
- *Tik tiems, kas naudoja Vagrant*
  - Sukuriam Vagrant box'ą šiam projektui
    - Sukuriam Vagrantfile (komanda `touch Vagrantfile`)
    - Nukopijuojame failo `https://github.com/cerkauskas/3wa-uzduotynas/blob/master/Vagrantfile` (komanda `wget -O - https://raw.githubusercontent.com/cerkauskas/3wa-uzduotynas/master/Vagrantfile > Vagrantfile`)
  - Paleidžiam Vagrant box'ą (komanda `vagrant up`)
  - Prisijungiam prie virtualios mašinos terminalo (komanda `vagrant ssh`)
  - Nunaviguojame į virtualios mašinos folderį, kuriame guli Laravel (komanda `cd /var/www`)
  - Patvarkome Vagrant box'o netobulumus Laraveliu
```php
# app/Providers/AppServiceProvider.php

use Illuminate\Support\Facades\Schema;

public function boot() {
    Schema::defaultStringLength(); // pridedame šitą eilutę
}
```
- Susikonfiguruojame duomenų bazę .env faile
  - Nustatome DB_DATABASE, DB_USERNAME ir DB_PASSWORD reikšmes
  - Jei naudojate Vagrant, tai jūsų prisijungimai bus tokie:
~~~~
DB_DATABASE=scotchbox
DB_USERNAME=root
DB_PASSWORD=root
~~~~
- Susikuriame blogo kategorijų CRUD'ą
  - Sugeneruojame Category modelį kartu su migracija (komanda `php artisan make:model Category -m`)
  - Aprašome migraciją
    - Atsidarome kątik sukurtos migracijos failą (`database/migrations/<...>_create_categories_table.php`)
    - `up()` metode pridedame papildomą lauką kategorijos antraštei saugot
```php
public function up()
{
    Schema::create('categories', function (Blueprint $table) {
        // ...
        $table->string('title');
        // ...
    });
}
```
  - Susigeneruojame resursinį kontrollerį (komanda `php artisan make:controller CategoryController --resource`)
  - Užregistruojame routeryje
```php
# router/web.php

//...

Route::resource("category", "CategoryController");

```
  - Sukuriame kategorijos kūrimo formą
    - `CategoryController` metodas `index` iškviečia templeitą ir parodo formą
      - Folderyje `resources/views` sukuriame dar vieną folderį `category` (komanda `mkdir resources/views/category`) 
      - Kątik sukurtame folderyje, sukuriam failą `create.blade.php`
      - `create.blade.php` faile sukuriame formą su vienu lauku (title)
      - Forma veda `/category` adresu `POST` metodu
    - `CategoryController` metodas `store` sukuria naują kategoriją ir išsaugo ją į duomenų bazę
      - Sukuriame naują `Category` tipo objektą
      - Į `title` propertį įrašome iš formos atėjusius duomenis
      - Išsaugome objektą į duomenų bazę
      - Nukreipiame vartotoją į kategorijų sąrašą (`/category`)
```php
# app/Http/Controllers/CategoryController.php

use App\Category;

public function store(Request $request)
{
    $category = new Category();
    $category->title = $request->get('title');

    $category->save();

    return redirect()
        ->to('category');
}
```


### Komandų paaiškinimai
##### `composer create-project . --prefer-dist`
- `composer create-project <...>` liepia composeriui kurti naują projektą
- `<...> . <...>` (taškas) reiškia, kad instaliuotų į darbinį (tą, kuriame esame) folderį (šiuo aveju `laravel-blog`)
- `<...> --prefer-dist` sako, kad jei įmanoma paimti ką nors iš cache, tai tegul iš ten ir ima; jei ne - bandyti parsisiųsti iš interneto

##### `touch Vagrantfile`
- `touch <...>` yra komanda `touch`, sukurianti naują tuščią failą
- `<...> Vagrantfile` nurodome failo pavadinimą

Jei norėtume sukurti `labas.txt` failą, tai kviestume komandą `touch labas.txt`

##### `wget -O - https://raw.githubusercontent.com/cerkauskas/3wa-uzduotynas/master/Vagrantfile > Vagrantfile`
- `wget <...>` yra komanda, parsiunčianti failus iš interneto
- `<...> -O <...>` yra komandos `wget` parametras, kuris reiškia, kad rezultatą rašytų į terminalo streamą
- `<...> - https://raw.githubusercontent.com/cerkauskas/3wa-uzduotynas/master/Vagrantfile <...>` failo adresas internete
- `<...> > <...>` ženklas > reiškia, kad rašysime į failą
- `<...> Vagrantfile` nurodome failo pavadinimą, į kurį rašysime